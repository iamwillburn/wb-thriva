# Notes

I haven’t used Pug since it was called Jade, so it felt like a bit of a throwback, I assume Pug was chosen instead of standard Vue.js templates due to familiarity of working with Ruby, but it would be interesting here exactly why the choice was made.

I’m sure I wasn’t supposed to, but I reworked the interface between DobInput and it’s parent component to hopefully be a little bit more straightforward and clear as I ran into an issue that became a pain to debug as it originally was.

I didn’t want the error message for DobInput to instantly be visible on the initial input: it felt jarring to see an error message pop up for the ‘year’ input midway through typing ‘1989’, so I added a bit more code to handle that behaviour. I’m sure there may be a more elegant solution for what I did as my familiarity with `vee-validate` likely hindered me here: I usually do my validation without a plugin by using a computed value on the component.

I was tempted to add a check if ‘Name’ had a value on each stage, if it didn’t I would bump the user back to the first stage of the survey to ensure all the fields would be filled out. However this felt like I was beginning to go beyond the scope of the exercise, so I stopped myself.
