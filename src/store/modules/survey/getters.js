export default {
  getSurveyData: (state) => {
    /*
      NOTE:
      I wasn't sure what shape of data the backend would be expecting, but it would
      be trivial enough to transform `goals` from an object to an array of strings.
    */
    return () => ({
      diet: state.diet,
      dob: state.dob,
      gender: state.gender,
      goals: { ...state.goals },
      name: state.name
    })
  }
}
