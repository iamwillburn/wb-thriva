export default {
  setCurrentStage (state, currentStage) {
    state.currentStage = currentStage
  },
  setDiet (state, diet) {
    state.diet = diet
  },
  setDob (state, dob) {
    state.dob = dob
  },
  setGender (state, gender) {
    state.gender = gender
  },
  setGoalSelection (state, [goal, value]) {
    const newGoals = {
      ...state.goals
    }
    newGoals[goal] = value
    state.goals = newGoals
  },
  setName (state, name) {
    state.name = name
  },
  setTransitionDirection (state, direction) {
    state.transitionDirection = direction
  }
}
