const state = () => ({
  currentStage: '',
  diet: '',
  dob: '',
  gender: '',
  goals: {},
  name: '',
  transitionDirection: 'forward'
})

export default state
