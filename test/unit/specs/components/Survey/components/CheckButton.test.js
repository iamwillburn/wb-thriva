import { mount } from '@vue/test-utils'
import CheckButton from '@/components/Survey/components/CheckButton'

describe('CheckButton', () => {
  let wrapper

  beforeEach(() => {
    wrapper = mount(CheckButton, {
      propsData: {
        text: 'Test Text',
        value: true
      }
    })
  })

  it('renders as it previously did', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })

  describe('When the value is `true`', () => {
    let wrapper

    beforeEach(() => {
      wrapper = mount(CheckButton, {
        propsData: {
          text: 'Test Text',
          value: true
        }
      })
    })

    it('has the selected class', () => {
      expect(wrapper.classes()).toContain('check-button--selected')
    })

    describe('When the user clicks on the button', () => {
      it('emits `false` as the value', () => {
        wrapper.trigger('click')
        expect(wrapper.emitted().input[0][0]).toEqual(false)
      })
    })
  })

  describe('When the value is `false`', () => {
    let wrapper

    beforeEach(() => {
      wrapper = mount(CheckButton, {
        propsData: {
          text: 'Test Text',
          value: false
        }
      })
    })

    it('does not have the selected class', () => {
      expect(wrapper.classes()).not.toContain('check-button--selected')
    })

    describe('When the user clicks on the button', () => {
      it('emits `true` as the value', () => {
        wrapper.trigger('click')
        expect(wrapper.emitted().input[0][0]).toEqual(true)
      })
    })
  })

  describe('When `disabled` is `true`', () => {
    let wrapper

    beforeEach(() => {
      wrapper = mount(CheckButton, {
        propsData: {
          disabled: true,
          text: 'Test Text',
          value: false
        }
      })
    })

    it('has the disabled class', () => {
      expect(wrapper.classes()).toContain('check-button--disabled')
    })

    describe('When the user clicks on the button', () => {
      it('no event is emitted', () => {
        wrapper.trigger('click')
        expect(wrapper.emitted()).toEqual({})
      })
    })
  })
})
