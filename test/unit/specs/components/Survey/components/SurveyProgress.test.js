import { mount } from '@vue/test-utils'
import SurveyProgress from '@/components/Survey/connectors/SurveyProgress'

describe('SurveyProgress', () => {
  const tests = [
    {
      currentStage: 'name',
      expectedCurrentStep: 1
    },
    {
      currentStage: 'goals',
      expectedCurrentStep: 2
    },
    {
      currentStage: 'diet',
      expectedCurrentStep: 3
    },
    {
      currentStage: 'dob',
      expectedCurrentStep: 4
    },
    {
      currentStage: 'gender',
      expectedCurrentStep: 5
    }
  ]
  for (const test of tests) {
    it(`computes the correct \`currentStep\` when \`currentStage\` is ${test.currentStage}`, () => {
      const wrapper = mount(SurveyProgress, {
        mocks: {
          $store: {
            state: {
              survey: {
                currentStage: test.currentStage
              }
            }
          }
        }
      })
      expect(wrapper.vm.currentStep).toEqual(test.expectedCurrentStep)
    })
  }
})
